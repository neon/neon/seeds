Task-Per-Derivative: 1
Task-Section: user
Task-Description: Neon desktop
Task-Extended-Description: KDE's Plasma Desktop
Task-Key: neon-desktop
Task-Seeds: desktop-common

= Hardware and Architecture Support =

 * (linux-generic-hwe-24.04)

== Architecture-independent ==

 * pm-utils

Bluetooth:

 * (libspa-0.2-bluetooth)        # Bluetooth audio devices by default in pipewire

= Network Services =

Basic network services and Windows integration.

 * (avahi-autoipd)     # IPv4 link-local interface configuration support
 * (mobile-broadband-provider-info)

= GUI infrastructure =

A11y components:

 * orca
 * brltty-speechd

Input methods:

# * fcitx5-frontend*
# * fcitx5-modules
# * (ibus-qt4)  # ibus support

Fonts:
 * fonts-noto-hinted
 * fonts-noto-color-emoji
 * fonts-hack-ttf
 * fonts-symbola

== Phonon and Frameworks ==

 * pipewire
 * wireplumber
 * (phonon4qt5-backend-vlc)
 * (phonon4qt6-backend-vlc)
 * (frameworkintegration)
 * (kf6-frameworkintegration)
 * (kimageformat-plugins)
 * (kf6-kimageformat-plugins)
 * (qt5-image-formats-plugins)
 * (qt6-image-formats-plugins)

== Plasma ==

 * breeze
 * breeze5
 * kde-style-breeze5
 * kde-cli-tools
 * kf6-baloo
 * kf6-kio
 * kinfocenter
 * kinit
 * kio
 * kio-extras
 * kio-extras5
 * kmenuedit
 * krdp
 * kwin
 * kwrited
 * oxygen-sounds
 * ocean-sound-theme
 * plasma-desktop
 * plasma-integration
 * plasma-integration5
 * plasma-systemmonitor
 * powerdevil
 * systemsettings
 * (plasma-calendar-addons)
 * (kde-config-gtk-style) #GTK setup
 * (kde-config-gtk-style-preview) # brings in the gtk libraries
 * (plasma-discover)
 * (polkit-kde-agent-1) # needed by discover but is an option compared to gtk polkit for gnome users
 * (ksshaskpass)
 * (bluedevil)
 * (kdeplasma-addons)
 * (kwin-addons)
 * (plasma-dataengines-addons)
 * (plasma-runners-addons)
 * (plasma-wallpapers-addons)
 * (kscreen)
 * (milou)
 * (plasma-nm)
 * (kde-config-sddm)
 * (plasma-pa)
 * (plymouth-theme-breeze)
 * (grub-theme-breeze)
 * (kde-config-plymouth)
 * (plasma-vault)
 * (plasma-workspace-wayland)
 * (plasma-browser-integration)
 * (plasma-thunderbolt)
 * (plasma-disks)
 * (plasma-firewall)
 * (plasma-systemmonitor)
 * (qml6-module-org-kde-breeze)
 * (qml6-module-org-kde-qqc2breezestyle)
 * (plasma-welcome)
 * (flatpak-kcm)
 * kaccounts-providers

= Applications

 * (dolphin)
 * (dolphin-plugins)
 * (konsole)
 * (kate)
 * (vlc)
 * (ark)
 * (print-manager)
 * (gwenview)
 * (kde-spectacle)
 * (okular)
 * (firefox) # needs GTK fixed up
 * (kdialog)
 * (kdegraphics-thumbnailers)
 * (kio-fuse)
 * (kde-inotify-survey)
 * (kio-admin)
 * (kio-gdrive)
 * (kio-gdrive-5)
 * (ffmpegthumbs)
 * (xwaylandvideobridge)
 * (kdeconnect)
 * (kwalletmanager)

= Other =

 * (cryptsetup) #needed for encrypted devices, also in d-i-requirements seed but might be needed for 3rd party drives
 # According to kbroulik this is needed to have RTL languages supported
 # properly, as such it shouldn't be removed as it can cause weird behavior.
 # With RTL not actually being applied.
 * qttranslations5-l10n
 * qt6-translations
 * lvm2

== supporting bits ==
 * (libpam-fprintd)     #
 * (libpam-sss)         # kscreenlocker pam modules
 * (libpam-pkcs11)      #
 * (upower)
 * (udisks2)
 * (xdg-utils)         # useful utilities
 * (cdrdao)
 * (libqca2-plugin-ossl)
 * (qml-module-qtquick-xmllistmodel)
 * (qml-module-qtgraphicaleffects)
 * xdg-user-dirs
 * sddm-theme-breeze
 * neon-settings-2
 * neon-keyring
 * (power-profiles-daemon)
 * switcheroo-control
 * (software-properties-common)
 * fonts-noto-hinted # Plasma's default font, we want this!
 * fonts-hack-ttf
 * (desktop-file-utils) # used by appimage https://github.com/blue-systems/pangea-tooling/issues/4
 * neon-hardware-integration # UDev rules applying hardware fixes where necessary.
 * docker-neon
 * (drkonqi-pk-debug-installer) # Debug installer for drkonqi
 * distro-release-notifier
 * ubuntu-release-upgrader-qt
 * (appimagelauncher)
 * (libappimage0) # from ubuntu needed by some appimages
 * (kde-nomodeset) # GUI to deal with the (Safe Graphics) boot mode on ISO + install
 * (neon-essentials-desktop) # marks certain packages as essential so apt is less inclined to remove them
 * (neon-boot-space) # tool to ensure there's enough space in /boot to apply kernel updates
 * (libmaliit-plugins2)     #
 * (maliit-keyboard)        # ensure onscreen keyboard works out of the box
 * (qt6-virtualkeyboard)    #
 * (xdg-desktop-portal-gtk) # wanted in /usr/share/xdg-desktop-portal/kde-portals.conf
 * (xsettingsd) # Also wanted by GTK apps on Plasma

== Blacklist ==

libavcodec cannot be shipped on CDs (c.f. Ubuntu technical board resolution 2007-01-02).

 * !libavcodec*
 * (!fonts-guru)

== Oops handling ==

Disable oops handling by default. As of kcrash 5.29 oops are automatically forwarded to handlers, which by default would be apport, which we do not want. coredumpd might be handy, but is not used right now https://phabricator.kde.org/T3891

We will thus mangle the output by sedding out the following: kerneloops-daemon, whoopsie, apport.
This happens in our ./update script!

As of 2019-05-13 unstable ships with coredumpd to ensure we can easily track down crashers that are much more likely to happen on git master.

 * (systemd-coredump)

== Ubuntu Masking ==

Ubuntu has some excessively useless deps deep in the stack. We'll "mask" them
with our own empty versions to get rid of them.

 * (neon-adwaita) # https://phabricator.kde.org/T8363
 * (neon-ubuntu-advantage-tools) # https://phabricator.kde.org/T8633
 * (neon-apport) # https://phabricator.kde.org/T8659

== Sanity ==

 # Recommend vim so Harald doesn't smash his keyboard.
 * (vim)
 # Recommend curl because openqa/os-autoinst uses curl to upload artifacts from
 # the VM to the test driver.
 * (curl)
